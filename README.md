### Dungeon generation

#### Dependence
* [Pillow](https://python-pillow.org/)

#### Description
Just a little dungeon generator. Just launch \_\_main\_\_.py and check your new dungeon.  
The map_objects package is part of roguelike game [tutorial](http://www.roguebasin.com/index.php?title=Complete_Roguelike_Tutorial,_using_python3%2Blibtcod) with libtcod library
