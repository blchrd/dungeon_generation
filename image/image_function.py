import logging
from pathlib import Path

logger = logging.getLogger('default')


def get_file_num(directory, extension):
    i = 0
    index = str(i).rjust(4, "0")
    file_name = directory + index + '.' + extension
    file = Path(file_name)
    while file.is_file():
        i += 1
        index = str(i).rjust(4, "0")
        file_name = directory + index + '.' + extension
        file = Path(file_name)

    return index
