import logging

from PIL import Image, ImageFont, ImageDraw

from image import image_function
from map_objects.game_map import GameMap

logger = logging.getLogger('default')


class GenerateDungeon:
    font_size = 16

    @staticmethod
    def generate_dungeon():
        dungeon_map = GameMap(width=79, height=41, winding_percent=25)
        dungeon_map.make_map(max_rooms=80, room_min_size=6, room_max_size=12, map_width=79, map_height=41)

        index = str(image_function.get_file_num('dungeons/', 'jpg'))
        file_name = 'dungeons/' + index + '.jpg'
        img = Image.open('resources/black.jpg')
        draw = ImageDraw.Draw(img)
        font = ImageFont.truetype("resources/Code New Roman.otf", GenerateDungeon.font_size)

        GenerateDungeon.draw_dungeon(dungeon_map, draw, font)
        draw.text((16, 44 * 16), "Dungeon #" + index, (255, 255, 255), font=font)

        img.save(file_name)
        return file_name

    @staticmethod
    def draw_dungeon(map_to_draw, draw, font):
        for x in range(81):
            draw.text((x * GenerateDungeon.font_size, 0), "#", (255, 255, 255), font=font)
            draw.text((x * GenerateDungeon.font_size, 42 * GenerateDungeon.font_size), "#", (255, 255, 255), font=font)
        for y in range(43):
            draw.text((0, y * GenerateDungeon.font_size), "#", (255, 255, 255), font=font)
            draw.text((80 * GenerateDungeon.font_size, y * GenerateDungeon.font_size), "#", (255, 255, 255), font=font)
        for x in range(map_to_draw.width):
            draw_x = (x + 1) * GenerateDungeon.font_size
            for y in range(map_to_draw.height):
                draw_y = (y + 1) * GenerateDungeon.font_size
                if map_to_draw.tiles[x][y].blocked:
                    draw.text((draw_x, draw_y), "#", (255, 255, 255), font=font)
                # uncomment if you want dot for the floor
                # else:
                #     draw.text((draw_x, draw_y), ".", (255, 255, 255), font=font)
