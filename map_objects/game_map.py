from random import randint, shuffle

from map_objects.position import Position
from map_objects.rectangle import Rect
from map_objects.tile import Tile


class GameMap:
    # CONSTANT FOR DUNGEON GENERATION
    DX = {'E': 1, 'W': -1, 'N': 0, 'S': 0}
    DY = {'E': 0, 'W': 0, 'N': -1, 'S': 1}

    def __init__(self, width, height, dungeon_level=1, debug=False, extra_connector_chance=20, room_extra_size=0,
                 winding_percent=40):
        self.width = width
        self.height = height
        self.tiles = self.initialize_tiles(debug)

        self.regions = [[-1 for y in range(self.height)] for x in range(self.width)]
        self.current_region = -1

        self.extra_connector_chance = extra_connector_chance
        self.room_extra_size = room_extra_size
        self.winding_percent = winding_percent

        self.dungeon_level = dungeon_level

    def initialize_tiles(self, debug):
        map_debug = debug
        tiles = [[Tile(True, explored=map_debug) for y in range(self.height)] for x in range(self.width)]

        return tiles

    def make_map(self, max_rooms, room_min_size, room_max_size, map_width, map_height):
        # http://journal.stuffwithstuff.com/2014/12/21/rooms-and-mazes/
        # https://github.com/munificent/hauberk/blob/db360d9efa714efb6d937c31953ef849c7394a39/lib/src/content/dungeon.dart
        self.regions = [[-1 for y in range(self.height)] for x in range(self.width)]
        self.current_region = -1

        # First we create the rooms
        self.make_rooms(max_rooms, room_min_size, room_max_size, map_width, map_height)

        # Then we make the mazes
        for y in range(0, self.height, 2):
            for x in range(0, self.width, 2):
                if self.tiles[x][y].blocked:
                    self.make_maze(x, y)

        # We connect all the regions and remove dead-ends
        self.connect_regions()
        # self.remove_dead_ends()

    def make_rooms(self, max_rooms, room_min_size, room_max_size, map_width, map_height):
        rooms = []
        num_rooms = 0
        center_of_last_room_x = None
        center_of_last_room_y = None

        for r in range(max_rooms):
            # random width and height
            w = int(randint(room_min_size, room_max_size) / 2) * 2
            h = int(randint(room_min_size, room_max_size) / 2) * 2
            # random position without going out of the boundaries of the map
            x = int(randint(0, map_width - w - 1) / 2) * 2 + 1
            y = int(randint(0, map_height - h - 1) / 2) * 2 + 1

            # "Rect" class makes rectangles easier to work with
            new_room = Rect(x, y, w, h)

            # run through the other rooms and see if they intersect with this one
            for other_room in rooms:
                if new_room.intersect(other_room):
                    break
            else:
                # this means there are no intersections, so this room is valid
                self.current_region += 1

                # "paint" it to the map's tiles
                self.create_room(new_room)

                # finally, append the new room to the list
                rooms.append(new_room)

            num_rooms += 1

    def make_maze(self, start_x, start_y):
        # Growing tree algorithm : http://www.astrolog.org/labyrnth/algrithm.htm
        directions = ['N', 'S', 'E', 'W']
        cells = []
        last_dir = ''

        self.current_region += 1
        self.carve(start_x, start_y)
        cells.append(Position(start_x, start_y))

        while len(cells) > 0:
            cell = cells[-1]
            unmade_cells = []

            for direction in directions:
                if self.can_carve(cell.x, cell.y, direction):
                    unmade_cells.append(direction)

            shuffle(unmade_cells)
            if len(unmade_cells) > 0:
                # Based on how "windy" passages are, try to prefer carving in the same direction.
                if unmade_cells.__contains__(last_dir) and randint(1, 100) < self.winding_percent:
                    direction = last_dir
                else:
                    direction = unmade_cells[0]

                nx = cell.x + GameMap.DX[direction]
                ny = cell.y + GameMap.DY[direction]
                self.carve(nx, ny)
                nx += GameMap.DX[direction]
                ny += GameMap.DY[direction]
                self.carve(nx, ny)
                cells.append(Position(nx, ny))

                last_dir = direction
            else:
                cells.__delitem__(-1)
                last_dir = ''

    def connect_regions(self):
        directions = ['N', 'S', 'E', 'W']
        connector_regions = []

        # Find all of the tiles that can connect two (or more) regions
        for y in range(0, self.height):
            for x in range(0, self.width):
                regions_connected = []

                if self.tiles[x][y].blocked:
                    for direction in directions:
                        cx = x + GameMap.DX[direction]
                        cy = y + GameMap.DY[direction]
                        if 0 < cx < self.width - 1 and 0 < cy < self.height:
                            region = self.regions[cx][cy]
                            if region >= 0 and not regions_connected.__contains__(region):
                                regions_connected.append(region)

                    if len(regions_connected) > 1:
                        connector_regions.append({'pos': Position(x, y), 'regions': regions_connected})

        # Keep track of which regions have been merged.
        # This maps an original region index to the one it has been merged to
        merged = [-1 for i in range(self.current_region)]
        open_regions = []
        for i in range(self.current_region):
            merged[i] = i
            open_regions.append(i)

        # Keep connecting regions until we're down to one
        # TODO : This code is bad, there is certainly so much ways to improve it
        while len(open_regions) > 0:
            shuffle(connector_regions)
            connector = None
            for connect in connector_regions:
                for region in connect.get('regions'):
                    if open_regions.__contains__(region):
                        connector = connect
                        break
                if connector is not None:
                    break
            if connector is None:
                break
            position_junction = connector.get('pos')

            # carve the connection
            self.add_junction(position_junction.x, position_junction.y)

            # Merge the connected regions, we'll pick on region and map all of the other regions to its index
            regions = connector.get('regions')
            dest = regions[0]
            sources = regions
            sources.remove(dest)

            # Merge all the affected regions
            for i in range(self.current_region):
                if sources.__contains__(merged[i]):
                    merged[i] = dest

            # Sources are no longer in use
            for region_merge in sources:
                if open_regions.__contains__(region_merge):
                    open_regions.remove(region_merge)

    def remove_dead_ends(self):
        directions = ['N', 'S', 'E', 'W']
        done = False

        while not done:
            done = True
            for y in range(self.height):
                for x in range(self.width):
                    if not self.tiles[x][y].blocked:
                        exits = 0
                        for direction in directions:
                            cx = x + GameMap.DX[direction]
                            cy = y + GameMap.DY[direction]
                            if not self.tiles[cx][cy].blocked:
                                exits += 1
                        if exits == 1:
                            done = False
                            self.uncarve(x, y)

    def create_room(self, room):
        # go through the tiles in the rectangle and make them passable
        for x in range(room.x1 + 1, room.x2):
            for y in range(room.y1 + 1, room.y2):
                self.carve(x, y)

    def can_carve(self, x, y, direction):
        nx = x + (GameMap.DX[direction] * 2)
        ny = y + (GameMap.DY[direction] * 2)
        # Must end in bounds
        if (nx < 0 or nx >= self.width) or (ny < 0 or ny >= self.height):
            return False

        # Destination must not be open
        nx = x + (GameMap.DX[direction] * 2)
        ny = y + (GameMap.DY[direction] * 2)
        return self.tiles[nx][ny].blocked

    def carve(self, x, y):
        self.regions[x][y] = self.current_region
        self.tiles[x][y].blocked = False
        self.tiles[x][y].block_sight = False

    def uncarve(self, x, y):
        self.tiles[x][y].blocked = True
        self.tiles[x][y].block_sight = True

    def add_junction(self, x, y):
        self.tiles[x][y].blocked = False
        self.tiles[x][y].block_sight = False

    def is_blocked(self, x, y):
        if self.tiles[x][y].blocked:
            return True

        return False
